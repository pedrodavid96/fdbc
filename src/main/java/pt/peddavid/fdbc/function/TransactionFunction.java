package pt.peddavid.fdbc.function;

import pt.peddavid.fdbc.ConnectionService;

import java.sql.SQLException;

@FunctionalInterface
public interface TransactionFunction<E> {

    E apply(ConnectionService trans) throws SQLException;

}
