package pt.peddavid.fdbc.statements;

import pt.peddavid.fdbc.ConnectionService;
import pt.peddavid.fdbc.connections.BaseConnection;
import pt.peddavid.fdbc.function.SQLConsumer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Update extends Statement<Update> {

    public Update(ConnectionService service, String query){
        this(service, query, (prepStmt) -> { });
    }

    protected Update(ConnectionService service, String query, SQLConsumer<PreparedStatement> psBuilder){
        super(service, query, psBuilder);
    }

    public int execute() throws SQLException {
        try (BaseConnection con = service.getConnection();
             PreparedStatement prepStmt = con.get().prepareStatement(query)
        ) {
            psSetter.set(prepStmt);
            return prepStmt.executeUpdate();
        }
    }

    public long fetchGeneratedKeys() throws SQLException {
        try (BaseConnection con = service.getConnection();
             PreparedStatement pstm = con.get().prepareStatement(query, java.sql.Statement.RETURN_GENERATED_KEYS)
        ) {
            psSetter.set(pstm);
            pstm.executeUpdate();
            ResultSet rs = pstm.getGeneratedKeys();
            if(rs != null && rs.next()){
                return rs.getLong(1);
            }
            return -1;
        }
    }

}
